//
//  ResultView.swift
//  calc
//
//  Created by Ionut Dirlea on 01/03/2020.
//  Copyright © 2020 Ionut Dirlea. All rights reserved.
//

import SwiftUI

struct ResultView:View {
    @EnvironmentObject var calculatorVM:CalculatorVM
    
    var body: some View {
        VStack {
            Spacer()
            HStack {
                Spacer()
                Text(calculatorVM.result)
                    .foregroundColor(Color.white)
                    .font(.system(size: CGFloat(calculatorVM.fontSize)))
                    .padding(.trailing, 30)
            }
        }
    }
}
