//
//  ContentView.swift
//  calc
//
//  Created by Ionut Dirlea on 01/03/2020.
//  Copyright © 2020 Ionut Dirlea. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var calculatorVM:CalculatorVM = CalculatorVM()
    
    var body: some View {
        ZStack {
            Rectangle()
                .foregroundColor(Color.black)
                .edgesIgnoringSafeArea(.all)
            VStack {
                ResultView()
                KeyPadView()
            }
        }.environmentObject(calculatorVM)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
